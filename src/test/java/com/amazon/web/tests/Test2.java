package com.amazon.web.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test2 extends TestBase {

  @Test
  public void testSearchBar() {
    app.getLandingPage();
    app.searchForItem();
    app.verifySearchResult();
    String[] guitars = {"Hollow & Semi-Hollow Body", "Lap & Pedal Steel Guitars", "Solid Body"};
    Set<String> verifySearchAgainst = Stream.of(guitars)
            .collect(Collectors.toCollection(HashSet::new));
    Assert.assertEquals(verifySearchAgainst, app.electricGuitarsTab());
  }
}
