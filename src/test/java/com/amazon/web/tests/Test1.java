package com.amazon.web.tests;

import org.testng.annotations.Test;

public class Test1 extends TestBase {

  @Test
  public void testIterateOverSomePages() {
    app.getDepartmentPage();
    app.getMusicalInstrumentPage();
    app.getGuitarPage();
    app.getElectricGuitarPage();
  }
}
