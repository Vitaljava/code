package com.amazon.web.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class ApplicationManager {

  private NavigationHelper navigationHelper;
  private final Properties properties;
  public FirefoxDriver wd;

  public ApplicationManager() {
    properties = new Properties();
  }

  public void init() throws IOException {
    properties.load(new FileReader(new File("src/test/resources/local.properties")));
    wd = new FirefoxDriver();
    wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    navigationHelper = new NavigationHelper(wd);
    getLandingPage();
  }

  public void getDepartmentPage() {
    navigationHelper.click(By.cssSelector("#nav-shop a"));
    navigationHelper.waitForTitleToBe("Amazon.com - Earth's Biggest Selection");
    assertTrue(navigationHelper.titleText(By.cssSelector("h1")).equals("Earth's biggest selection"));
  }

  public void getMusicalInstrumentPage() {
    navigationHelper.click(By.xpath("//a[contains(text(), 'Musical Instruments')][1]"));
    navigationHelper.waitForTitleToBe("Amazon.com: Musical Instruments");
    assertTrue(navigationHelper.titleText(By.cssSelector("h1.browse-node__title")).equals("MUSICAL\nINSTRUMENTS"));
  }

  public void getGuitarPage() {
    navigationHelper.doubleClick(By.cssSelector("#cat-0 .category-link__text"));
    navigationHelper.waitForTitleToBe("Shop Amazon.com | Guitars");
    assertTrue(navigationHelper.titleText(By.cssSelector("h1.browse-node__title")).equals("GUITARS"));
  }

  public void getElectricGuitarPage() {
    navigationHelper.doubleClick(By.cssSelector("#cat-0 .category-link__text"));
    navigationHelper.waitForTitleToBe("Shop Amazon.com | Electric Guitars");
    assertTrue(navigationHelper.titleText(By.cssSelector("h1")).equals("Electric Guitars"));
  }

  public void getLandingPage() {
    wd.get(properties.getProperty("base.Url"));
  }

  public void stop() {
    wd.quit();
  }

  public String item = "Electric Guitars";

  public void searchForItem() {
    navigationHelper.typeAndSend(By.cssSelector("#twotabsearchtextbox"), item);
  }

  public void verifySearchResult() {
    assertTrue(wd.findElement(By.cssSelector("#s-result-count")).getText().contains(item));
  }

  public Set<String> electricGuitarsTab() {
    Set<String> guitars = new HashSet<>();
    List<WebElement> elements = wd.findElements(By.cssSelector("ul[class*='s-ref-indent-two'] a"));
    for (WebElement e : elements) {
      guitars.add(e.getText());
    }
    return guitars;
  }

  public NavigationHelper getNavigationHelper() {
    return navigationHelper;
  }
}
