package com.amazon.web.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NavigationHelper {
  private FirefoxDriver wd;

  public NavigationHelper(FirefoxDriver wd) {
    this.wd = wd;
  }

  public void waitForTitleToBe(String title) {
    WebDriverWait wait = new WebDriverWait(wd, 5);
    wait.until(ExpectedConditions.titleIs(title));
  }

  public void click(By locator) {
      WebElement element = wd.findElement(locator);
      if (element.isDisplayed()) {
          element.click();
      }
  }

  public void typeAndSend(By locator, String text) {
    click(locator);
    if (text != null) {
      String existingText = wd.findElement(locator).getAttribute("value");
      if (!existingText.equals(text)) {
        wd.findElement(locator).clear();
        wd.findElement(locator).sendKeys(text + Keys.ENTER);
      }
    }
  }

  public void doubleClick(By locator) {
      Actions actions = new Actions(wd);
      WebElement element = wd.findElement(locator);
      actions.moveToElement(element).doubleClick().perform();
  }

  public String titleText(By locator) {
      return wd.findElement(locator).getText();
  }
}
